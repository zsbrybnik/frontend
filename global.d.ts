declare module "duplicate-package-checker-webpack-plugin" {
  declare class DuplicatePackageCheckerPlugin {}
  export default DuplicatePackageCheckerPlugin;
}
declare module "@mdx-js/runtime" {
  declare const MDX: FunctionComponent<PropsWithChildren>;
  export default MDX;
}
declare module "json-minimizer-webpack-plugin";
declare module "script-ext-html-webpack-plugin";
declare module "interpolate-html-plugin";
declare module "native-addon-rust";
declare module "robotstxt-webpack-plugin";
declare module "@vue/preload-webpack-plugin";
declare module "*.svg";
