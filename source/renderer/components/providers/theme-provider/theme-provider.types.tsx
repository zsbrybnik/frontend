import { PropsWithChildren, FunctionComponent } from "react";

export type ThemeProviderProperties = PropsWithChildren;

export type ThemeProviderComponent = FunctionComponent<ThemeProviderProperties>;
