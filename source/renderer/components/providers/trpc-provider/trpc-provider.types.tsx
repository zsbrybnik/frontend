import { PropsWithChildren, FunctionComponent } from "react";

export type TRPCProviderProperties = PropsWithChildren;

export type TRPCProviderComponent = FunctionComponent<TRPCProviderProperties>;
