enum IpcEvents {
  HardReload = "hardReload",
  Reload = "reload",
  ToggleDevelopmentTools = "toggleDevelopmentTools",
  Close = "close",
  Minimize = "minimize",
  ToggleMaximize = "toggleMaximize",
  ToggleFullscreen = "toggleFullscreen",
  ExtractToSeparateWindow = "extractToSeparateWindow",
}

export default IpcEvents;
